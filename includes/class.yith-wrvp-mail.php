<?php
/**
 * Email class
 *
 * @author Yithemes
 * @package YITH WooCommerce Recently Viewed Products
 * @version 1.0.0
 */

if ( ! defined( 'YITH_WRVP' ) ) {
	exit;
} // Exit if accessed directly

if ( ! class_exists( 'YITH_WRVP_Mail' ) ) {
	/**
	 * Email Class
	 * Extend WC_Email to send mail to customer
	 *
	 * @class   YITH_WRVP_Mail
	 * @extends  WC_Email
	 */
	class YITH_WRVP_Mail extends WC_Email {

		/**
		 * Msg type for test mail
		 *
		 * @var string
		 */
		public $_test_msg_type = 'error';

		/**
		 * Constructor
		 *
		 * @author Francesco Licandro <francesco.licandro@yithemes.com>
		 */
		public function __construct() {

			$this->id           = 'yith_wrvp_mail';
			$this->title        = __( 'YITH Recently Viewed Products Email', 'yith-woocommerce-recently-viewed-products' );
			$this->customer_email = true;
            $this->description = '';

			$this->heading      = __( '{blogname}', 'yith-woocommerce-recently-viewed-products' );
			$this->subject      = __( 'You may be interested in these products.', 'yith-woocommerce-recently-viewed-products' );
			$this->mail_content = __( 'According to your research, you may be interested in the following products. Moreover, purchasing one of these products will entitle you to receive a discount with the following coupon {coupon_code}{products_list}', 'yith-woocommerce-recently-viewed-products' );

			$this->template_base    = YITH_WRVP_TEMPLATE_PATH . '/email/';
			$this->template_html    = 'ywrvp-mail-template.php';
			$this->template_plain   = 'plain/ywrvp-mail-template.php';

			// Triggers for this email
			add_action( 'send_yith_wrvp_mail_notification', array( $this, 'trigger' ), 10, 1 );

			// filter style for email
			add_filter( 'woocommerce_email_styles', array( $this, 'my_email_style' ), 10, 1 );

			// Call parent constructor
			parent::__construct();
		}

		/**
		 * Trigger Function
		 *
		 * @access public
		 * @since 1.0.0
		 * @param mixed $data
		 * @return void
		 * @author Francesco Licandro <francesco.licandro@yithemes.com>
		 */
		public function trigger( $data ) {

			if ( ! $this->is_enabled() ) {
				return;
			}

			$is_test = false;

			// get option mail
			$content = $this->get_option('mail_content');
			$coupon_enabled = ( $this->get_option('coupon_enable' ) == 'yes' && get_option( 'woocommerce_enable_coupons' ) == 'yes' );
			$coupon_expire = time() + ( intval( $this->get_option( 'coupon_expiry' ) ) * DAY_IN_SECONDS );
			$coupon_value = $this->get_option( 'coupon_amount' );
			$custom_products = $this->get_option( 'custom_products' );
			$most_viewed_cat = $this->get_option( 'cat_most_viewed' ) == 'yes';

			$products_type = $this->get_option('products_type');

			// find logo image
			$this->find['logo-image'] = '{logo_image}';
			$this->replace['logo-image'] = $this->print_logo_image();

			foreach( $data as $customer => $products ) {

				// if products is empty else is test
				$is_test = empty( $products );

				$products_list = '';
				$custom_products_list = '';
				$coupon_code = '';
				$cat_id = false;
				$product_title = 'Product-Name';

				if ( ! $is_test ) {
					// most viewed cat
					if ( $most_viewed_cat ) {
						$cat_id = YITH_WRVP_Frontend_Premium()->most_viewed_cat( $products );
					}
					// get similar
					if ( $products_type == 'similar' ) {
						$products = YITH_WRVP_Frontend_Premium()->get_similar_products( array( $cat_id ), '', $products );
					}

					// set subject based on first product title
					if ( strpos( $this->subject, '{first_product_title}' ) >= 0) {

						$first_product = array_slice($products, 0, 1);
						if ( ! is_null( $first_product ) ) {
							$product_title = get_the_title( array_shift( $first_product ) );
						}

					}
				}

				// products list
				if ( strpos($content, '{products_list}') >= 0 ) {
					// get products list html;
					$products_list = $this->get_products_list_html( $products, false, $cat_id );
				}

				$this->find['products-list'] = '{products_list}';
				$this->replace['products-list'] = $products_list;
				$this->find['first-product'] = '{first_product_title}';
				$this->replace['first-product'] = $product_title;

                // user fields
                preg_match( '/\{customer_(.*?)\}/', $content, $customer_data );
                if( $customer_data ){
                    $customer_obj = get_user_by( 'email', $customer );
                    if( $customer ) {
                        foreach ( $customer_data as $data ) {
                            if ( isset( $customer_obj->$data ) ) {
                                $this->find["customer-$data"] = "{customer_$data}";
			                    $this->replace["customer-$data"] = $customer_obj->$data;
                            }
                        }
                    }
                }

				// coupon expire
				$this->find['coupon-expire'] = '{coupon_expire}';
				$this->replace['coupon-expire'] = date( 'Y-m-d', $coupon_expire );

				// coupon code
				if( $coupon_enabled && ( strpos( $content, '{coupon_code}' ) >= 0 ) ) {
					// create coupon
					$coupon_code = $is_test ? 'aaabbbccc' : YITH_WRVP_Mail_Handler()->add_coupon_to_mail( $customer, $products, $coupon_expire, $coupon_value );
				}

				$this->find['coupon-code'] = '{coupon_code}';
				$this->replace['coupon-code'] = $this->get_copuon_code_html( $coupon_code );

				// custom products list
				if( ( strpos( $content, '{custom_products_list}' ) >= 0 ) && ! empty( $custom_products ) ) {
					// get products list html;
                    ! is_array( $custom_products ) && $custom_products = explode( ',', $custom_products );
					$custom_products_list = $this->get_products_list_html( $custom_products, true );
				}

				$this->find['custom-products-list'] = '{custom_products_list}';
				$this->replace['custom-products-list'] = $custom_products_list;

				// search for unsubscribe link
				preg_match( '/\{{(.*?)\}}/', $content, $unsub_link );
				if( $unsub_link && isset( $unsub_link[1] ) ) {

					$this->find['unsubscribe-from-list'] = '{{'.$unsub_link[1].'}}';
					$this->replace['unsubscribe-from-list'] = $this->get_unsubscribe_link( $customer, $unsub_link[1], $is_test );
				}

				// send!
				if ( $this->send( $customer, $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() ) ) {
					do_action( 'yith_wrvp_mail_sent_correctly', $customer );
					$this->_test_msg_type = 'success';
				}
			}

			if( $is_test ) {
				add_action( 'woocommerce_email_settings_before', array( YITH_WRVP_Mail_Handler(), 'add_test_mail_message' ), 10, 1 );
			}
		}

		/**
		 * Send mail using standard WP Mail or Mandrill Service
		 *
		 * @access public
		 * @since 1.0.0
		 * @param string $to
		 * @param string $subject
		 * @param string $message
		 * @param string $headers
		 * @param string $attachments
		 *
		 * @return bool | void
		 * @author Francesco Licandro <francesco.licandro@yithemes.com>
		 */
		public function send( $to, $subject, $message, $headers, $attachments ) {

			// Retrieve Mandrill API KEY
			$api_key = get_option( 'yith-wrvp-mandrill-api-key' );

			if( get_option( 'yith-wrvp-use-mandrill' ) != 'yes' || empty( $api_key ) ) {
				return parent::send( $to, $subject, $message, $headers, $attachments );
			}
			else {

				/**
				 * Filter the wp_mail() arguments.
				 *
				 * @since 2.2.0
				 *
				 * @param array $args A compacted array of wp_mail() arguments, including the "to" email,
				 *                    subject, message, headers, and attachments values.
				 */
				$atts = apply_filters( 'wp_mail', compact( 'to', 'subject', 'message', 'headers', 'attachments' ) );

				if ( isset( $atts['to'] ) ) {
					$to = $atts['to'];
				}

				if ( isset( $atts['subject'] ) ) {
					$subject = $atts['subject'];
				}

				if ( isset( $atts['message'] ) ) {
					$message = $atts['message'];
				}

				if ( isset( $atts['headers'] ) ) {
					$headers = $atts['headers'];
				}

				if ( isset( $atts['attachments'] ) ) {
					$attachments = $atts['attachments'];
				}

				if ( ! is_array( $attachments ) ) {
					$attachments = explode( "\n", str_replace( "\r\n", "\n", $attachments ) );
				}

				// include lib
				include_once( YITH_WRVP_DIR . 'includes/third-party/Mandrill/Mandrill.php' );

				// Headers
				if ( empty( $headers ) ) {
					$headers = array();
				}
				else {
					if ( ! is_array( $headers ) ) {
						// Explode the headers out, so this function can take both
						// string headers and an array of headers.
						$tempheaders = explode( "\n", str_replace( "\r\n", "\n", $headers ) );
					}
					else {
						$tempheaders = $headers;
					}

					$headers = array();
					$cc = array();
					$bcc = array();

					// If it's actually got contents
					if ( ! empty( $tempheaders ) ) {
						// Iterate through the raw headers
						foreach ( (array) $tempheaders as $header ) {
							if ( strpos($header, ':') === false ) {
								if ( false !== stripos( $header, 'boundary=' ) ) {
									$parts = preg_split('/boundary=/i', trim( $header ) );
									$boundary = trim( str_replace( array( "'", '"' ), '', $parts[1] ) );
								}
								continue;
							}
							// Explode them out
							list( $name, $content ) = explode( ':', trim( $header ), 2 );

							// Cleanup crew
							$name    = trim( $name    );
							$content = trim( $content );

							switch ( strtolower( $name ) ) {
								// Mainly for legacy -- process a From: header if it's there
								case 'from':
									if ( strpos($content, '<' ) !== false ) {
										// So... making my life hard again?
										$from_name = substr( $content, 0, strpos( $content, '<' ) - 1 );
										$from_name = str_replace( '"', '', $from_name );
										$from_name = trim( $from_name );

										$from_email = substr( $content, strpos( $content, '<' ) + 1 );
										$from_email = str_replace( '>', '', $from_email );
										$from_email = trim( $from_email );
									} else {
										$from_email = trim( $content );
									}
									break;
								default:
									// Add it to our grand headers array
									$headers[trim( $name )] = trim( $content );
									break;
							}
						}
					}
				}

				// From email and name
				// If we don't have a name from the input headers
				if ( !isset( $from_name ) )
					$from_name = $this->get_from_name();

				// If we don't have an email from the input headers
				if ( !isset( $from_email ) ) {
					$from_email = $this->get_from_address();
				}

				// Set destination addresses
				if ( ! is_array( $to ) ){
					$to = explode( ',', $to );
				}

				$recipients = array();

				foreach ( (array) $to as $recipient ) {
					try {
						// Break $recipient into name and address parts if in the format "Foo <bar@baz.com>"
						$recipient_name = '';
						if( preg_match( '/(.*)<(.+)>/', $recipient, $matches ) ) {
							if ( count( $matches ) == 3 ) {
								$recipient_name = $matches[1];
								$recipient = $matches[2];
							}
						}
						$recipients[] = array(
							'email' => $recipient,
							'name' 	=> $recipient_name,
							'type' 	=> 'to'
						);
					}
					catch ( phpmailerException $e ) {
						continue;
					}
				}

				$files_to_attach = array();

				if( ! empty( $attachments ) ){
					foreach ( $attachments as $attachment ) {
						try {
							$new_attachment = $this->get_attachment_struct( $attachment );

							if( $new_attachment == false ){
								continue;
							}

							$files_to_attach[] = $new_attachment;
						} catch ( Exception $e ) {
							continue;
						}
					}
				}

				try{
					$mandrill = new Mandrill( $api_key );
					$message = apply_filters( 'ywrvp_mandrill_send_mail_message', array(
						'html' => apply_filters( 'woocommerce_mail_content', $this->style_inline( $message ) ),
						'subject' => $subject,
						'from_email' => apply_filters( 'wp_mail_from', $from_email ),
						'from_name' => apply_filters( 'wp_mail_from_name', $from_name ),
						'to' => $recipients,
						'headers' => $headers,
						'attachments' => $files_to_attach
					) );

					$async = apply_filters( 'ywrvp_mandrill_send_mail_async', false );
					$ip_pool = apply_filters( 'ywrvp_mandrill_send_mail_ip_pool', null );
					$send_at = apply_filters( 'ywrvp_mandrill_send_mail_send_at', null );

					$results = $mandrill->messages->send( $message, $async, $ip_pool, $send_at );
					$return = true;

					if( ! empty( $results ) ){
						foreach( $results as $result ){
							if( ! isset( $result['status'] ) || in_array( $result['status'], array( 'rejected', 'invalid' ) ) ){
								$return = false;
							}
						}
					}

					return $return;
				}
				catch( Mandrill_Error $e ) {
					return false;
				}
			}
		}

		/**
		 * Using file path, build an attachment struct, to use in Mandrill send request
		 *
		 * @param $path string File absolute path
		 *
		 * @static
		 * @throws Exception When some error occurs with file handling
		 * @return bool|array
		 * [
		 *     type => mime type of the file
		 *     name => file name with extension
		 *     content => file complete content, divided in chunks
		 * ]
		 * @since  1.0.0
		 */
		public static function get_attachment_struct( $path ) {

			$struct = array();

			try {
				if ( !@is_file($path) ) throw new Exception($path.' is not a valid file.');

				$filename = basename($path);

				$file_buffer  = file_get_contents($path);
				$file_buffer  = chunk_split( base64_encode( $file_buffer ), 76, "\n" );

				$mime_type = '';
				if ( function_exists('finfo_open') && function_exists('finfo_file') ) {
					$finfo = finfo_open( FILEINFO_MIME_TYPE );
					$mime_type = finfo_file( $finfo, $path );
				}
				elseif ( function_exists('mime_content_type') ) {
					$mime_type = mime_content_type($path);
				}

				if ( !empty( $mime_type ) ){
					$struct['type']     = $mime_type;
				}

				$struct['name']     = $filename;
				$struct['content']  = $file_buffer;

			} catch (Exception $e) {
				return false;
			}

			return $struct;
		}

		/**
		 * @return string|void
		 */
		public function init_form_fields() {

			parent::init_form_fields();

			if( isset( $this->form_fields['subject'] ) ) {
				$this->form_fields['subject']['desc_tip'] = sprintf( __( 'Use this placeholder to show the title of the first product %s', 'yith-woocommerce-recently-viewed-products'), '{first_product_title}');
			}

			$upload['upload_logo'] = array(
				'title'         => __( 'Logo image', 'yith-woocommerce-recently-viewed-products' ),
				'type'          => 'yith_wrvp_upload',
				'description'   => __( 'Upload logo image for email header. Use {logo_image} placeholder in the header to show it.', 'yith-woocommerce-recently-viewed-products' ),
				'default'       => ''
			);
			// move upload after mail header
			$this->form_fields = array_slice( $this->form_fields, 0, 3, true ) + $upload + array_slice( $this->form_fields, 3, count( $this->form_fields ) -1, true);

			// add other options
			$this->form_fields['mail_content'] = array(
				'title'         => __( 'Email content', 'yith-woocommerce-recently-viewed-products' ),
				'type'          => 'yith_wrvp_textarea',
				'description'   => sprintf( __( 'Defaults to <code>%s</code>. Add text between {{}} to make it an unsubscribe link.', 'yith-woocommerce-recently-viewed-products' ), $this->mail_content ),
				'placeholder'   => '',
				'default'       => $this->mail_content
			);
			$this->form_fields['custom_products'] = array(
				'title'         => __( 'Add custom products', 'yith-woocommerce-recently-viewed-products' ),
				'type'          => 'yith_wrvp_select_products',
				'description'   => __( 'Add custom products to the email', 'yith-woocommerce-recently-viewed-products' ),
				'default'       => ''
			);
			$this->form_fields['number_products'] = array(
				'title'         => __( 'Number of products', 'yith-woocommerce-recently-viewed-products' ),
				'type'          => 'number',
				'description'   => __( 'Choose how many products from users\' product list show in the email', 'yith-woocommerce-recently-viewed-products' ),
				'default'       => '5',
				'custom_attributes' => array(
					'min'	=> 0
				)
			);
			$this->form_fields['products_type'] = array(
				'title'         => __( 'Product type', 'yith-woocommerce-recently-viewed-products' ),
				'description'   => __( 'Select which type of products to add in email', 'yith-woocommerce-recently-viewed-products' ),
				'type'          => 'select',
				'options'			=> array(
					'viewed'	=> __( 'Only viewed products', 'yith-woocommerce-recently-viewed-products' ),
					'similar'	=> __( 'Include similar products', 'yith-woocommerce-recently-viewed-products' )
				),
				'default'       => 'viewed'
			);
			$this->form_fields['products_order'] = array(
				'title'         => __( 'Products ordered by', 'yith-woocommerce-recently-viewed-products' ),
				'description'   => __( 'Choose in which order you want to show products.', 'yith-woocommerce-recently-viewed-products' ),
				'type'          => 'select',
				'options'			=> array(
					'rand'		=> __( 'Random', 'yith-woocommerce-recently-viewed-products' ),
					'sales'		=> __( 'Sales', 'yith-woocommerce-recently-viewed-products' ),
					'newest'	=> __( 'Newest', 'yith-woocommerce-recently-viewed-products' ),
					'high-low'	=> __( 'Price: High to Low', 'yith-woocommerce-recently-viewed-products' ),
					'low-high'	=> __( 'Price: Low to High', 'yith-woocommerce-recently-viewed-products' ),
				),
				'default'       => 'rand'
			);
			$this->form_fields['cat_most_viewed'] = array(
				'title'         => __( 'Only the most viewed category', 'yith-woocommerce-recently-viewed-products' ),
				'description'   => __( 'Enable this option if you want to display only the products of the most viewed category.', 'yith-woocommerce-recently-viewed-products' ),
				'type'          => 'checkbox',
				'default'       => 'no'
			);

			// add coupon options
			if( get_option( 'woocommerce_enable_coupons' ) == 'yes' ) {

				$this->form_fields['title_coupon_section'] = array(
					'title' => __('Add coupon to email', 'yith-woocommerce-recently-viewed-products'),
					'type' => 'title'
				);

				$this->form_fields['coupon_enable'] = array(
					'title' => __('Enable coupon', 'yith-woocommerce-recently-viewed-products'),
					'label' => __('Create and add automatically a coupon for the products added in the email', 'yith-woocommerce-recently-viewed-products'),
					'type' => 'checkbox',
					'default' => 'yes',
				);

				$this->form_fields['coupon_amount'] = array(
					'title' => __('Coupon amount', 'yith-woocommerce-recently-viewed-products'),
					'description' => __('The coupon amount (Product % Discount).', 'yith-woocommerce-recently-viewed-products'),
					'type' => 'number',
					'default' => '',
					'placeholder' => '%',
					'custom_attributes' => array(
						'min' => 0,
						'max' => 100
					)
				);

				$this->form_fields['coupon_expiry'] = array(
					'title' => __('Coupon expiration date', 'yith-woocommerce-recently-viewed-products'),
					'description' => __('Set for how many days the coupon sent with the email can be used.', 'yith-woocommerce-recently-viewed-products'),
					'type' => 'number',
					'default' => '7',
					'custom_attributes' => array(
						'min' => 1
					)
				);
			}
		}

		/**
		 * Generate YITh Select products Input HTML.
		 *
		 * @param  mixed $key
		 * @param  mixed $data
		 * @since  1.0.0
		 * @return string
		 */
		public function generate_yith_wrvp_select_products_html( $key, $data ) {

			// get html
			$html = YITH_WRVP_Mail_Handler()->select_products_html( $key, $data, $this );

			return $html;
		}

		/**
		 * Return YITh Texteditor HTML.
		 *
		 * @param $key
		 * @param $data
		 * @return string
		 */
		public function generate_yith_wrvp_textarea_html( $key, $data ) {

			// get html
			$html = YITH_WRVP_Mail_Handler()->textarea_editor_html( $key, $data, $this );

			return $html;
		}

		/**
		 * Return YITh Upload HTML.
		 *
		 * @param $key
		 * @param $data
		 * @return string
		 */
		public function generate_yith_wrvp_upload_html( $key, $data ) {

			// get html
			$html = YITH_WRVP_Mail_Handler()->upload_html( $key, $data, $this );

			return $html;
		}

		/**
		 * get custom email content from options
		 *
		 * @access public
		 * @since 1.0.0
		 * @return string
		 * @author Francesco Licandro <francesco.licandro@yithemes.com>
		 */
		public function get_custom_option_content() {
			$content = $this->get_option( 'mail_content' );

			return $this->format_string( $content );
		}

		/**
		 * get_content_html function.
		 *
		 * @access public
		 * @since 1.0.0
		 * @return string
		 * @author Francesco Licandro <francesco.licandro@yithemes.com>
		 */
		public function get_content_html() {
			ob_start();

			wc_get_template( $this->template_html, array(
					'email_heading' => $this->get_heading(),
					'email_content' => $this->get_custom_option_content(),
					'email'         => $this
			), false, $this->template_base
			);

			return ob_get_clean();
		}

		/**
		 * get_content_plain function.
		 *
		 * @access public
		 * @since 1.0.0
		 * @return string
		 * @author Francesco Licandro <francesco.licandro@yithemes.com>
		 */
		public function get_content_plain() {
			ob_start();

			wc_get_template( $this->template_plain, array(
					'email_heading' => $this->get_heading(),
					'email_content' => $this->get_custom_option_content(),
					'email'         => $this
			), false, $this->template_base
			);

			return ob_get_clean();
		}

		/**
		 * Get products list html
		 *
		 * @access public
		 * @since 1.0.0
		 * @param array $products
		 * @param bool $is_custom
		 * @param string|bool $cat_id
		 * @return mixed
		 * @author Francesc Licandro
		 */
		public function get_products_list_html( $products, $is_custom = false, $cat_id = false ) {

			$args = apply_filters('yith_wrvp_similar_products_template_args', array(
				'post_type' => 'product',
				'ignore_sticky_posts' => 1,
				'post_status' => 'publish',
				'no_found_rows' => 1,
				'posts_per_page' => $this->get_option( 'number_products', '5' ),
				'order' => 'DESC'
			));

			if( ! empty( $products ) ) {
				$args['post__in'] = $products;
			}

			if( $cat_id ) {
				$args['tax_query'] = array(
					array(
						'taxonomy' => 'product_cat',
						'field' => 'id',
						'terms' => $cat_id
					)
				);
			}

			// hide free
			$args['meta_query'] = array(
				array(
					'key'     => '_price',
					'value'   => 0,
					'compare' => '>',
					'type'    => 'DECIMAL'
				)
			);

			if( get_option( 'yith-wrvp-hide-out-of-stock' ) == 'yes' ) {
				$args['meta_query'][] = array(
					array(
						'key' 		=> '_stock_status',
						'value' 	=> 'instock',
						'compare' 	=> '='
					)
				);
			}

			$order = $this->get_option( 'products_order', 'rand' );

			switch( $order ) {
				case 'sales':
					$args['meta_key'] = 'total_sales';
					$args['orderby']  = 'meta_value_num';
					break;
				case 'newest':
					$args['orderby'] = 'date';
					break;
				case 'high-low':
					$args['meta_key'] = '_price';
					$args['orderby']  = 'meta_value_num';
					break;
				case 'low-high':
					$args['meta_key'] = '_price';
					$args['orderby']  = 'meta_value_num';
					$args['order'] = 'ASC';
					break;
				default:
					$args['orderby']  = 'rand';
					break;
			}

			// visibility query condition
            $args = yit_product_visibility_meta( $args );

			$products = new WP_Query( $args );

			$template_name = $is_custom ? 'ywrvp-mail-custom-products-list.php' : 'ywrvp-mail-products-list.php';

			ob_start();

			if ( $products->have_posts() ) {

				wc_get_template( $template_name, array( 'products' => $products ), '', YITH_WRVP_TEMPLATE_PATH . '/email/' );
			}

			return ob_get_clean();
		}

		/**
		 * Filter email style and add custom style
		 *
		 * @access public
		 * @since 1.0.0
		 * @param string $css
		 * @return string
		 * @author Francesco Licandro
		 */
		public function my_email_style( $css ) {

			if( $this->id != 'yith_wrvp_mail' ) {
				return $css;
			}

			ob_start();
			wc_get_template( 'ywrvp-mail-style.php', array(), '', YITH_WRVP_TEMPLATE_PATH . '/email/' );
			$css .= ob_get_clean();

			return $css;
		}

		/**
		 * Get coupon code html
		 *
		 * @access public
		 * @since 1.0.0
		 * @param string $coupon_code
		 * @return string
		 * @author Francesco Licandro
		 */
		public function get_copuon_code_html( $coupon_code ) {

			if( ! $coupon_code ) {
				return '';
			}

			$coupon_image = apply_filters( 'yith_wrvp_coupon_code_image_email', YITH_WRVP_ASSETS_URL . '/images/coupon-code.png', $coupon_code );

			ob_start();
			?>

			<div id="coupon-code">
				<span style="background-image: url('<?php echo $coupon_image; ?>');"><?php echo $coupon_code ?></span>
			</div>

			<?php

			$return = ob_get_clean();
			return apply_filters( 'yith_wrvp_coupon_code_html_email', $return, $coupon_code, $coupon_image );
		}

		/**
		 * Print html for logo image
		 *
		 * @access public
		 * @since 1.0.0
		 * @author Francesco Licandro
		 */
		public function print_logo_image(){

			$logo = $this->get_option('upload_logo');

			if( empty( $logo ) ) {
				return '';
			}

			ob_start();
			?>

			<img src="<?php echo esc_url( $logo ) ?>" alt="<?php _e('Logo Image', 'yith-woocommerce-recently-viewed-products' ); ?>">

			<?php

			return ob_get_clean();
		}

		/**
		 * Get unsubscribe from mailing list link
		 *
		 * @access public
		 * @since 1.0.0
		 * @param $customer_mail
		 * @param $label
		 * @param $is_test
		 * @return string
		 * @author Francesco Licandro
		 */
		public function get_unsubscribe_link( $customer_mail, $label = '', $is_test = false ){

			$customer = get_user_by( 'email', $customer_mail );
			// if customer not exists return empty string
			if( ! $customer || $is_test ) {
				return '<a href="#">' . $label . '</a>';
			}

			$id = md5( $customer->ID );
			$url = apply_filters( 'yith_wrvp_unsubscribe_link_url', home_url() );

			$url = esc_url_raw( add_query_arg( array(
				'action' => 'yith_wrvp_unsubscribe_from_list',
				'customer' => $id ), $url ) );

			return '<a href="' . $url .'">' . $label . '</a>';
		}

		/**
         * Validate field select product
         *
         * @since 1.1.0
         * @author Francesco Licandro
         * @param string $key
         * @param mixed $value
         * @return mixed
         */
		public function validate_yith_wrvp_select_products_field( $key, $value ) {
		    return is_array( $value ) ? array_filter( $value ) : (string) $value;
        }
	}
}

return new YITH_WRVP_Mail();
