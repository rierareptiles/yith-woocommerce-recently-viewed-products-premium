<?php
/**
 * Plugin Utility Functions
 *
 * @author Yithemes
 * @package YITH WooCommerce Recently Viewed Products
 * @version 1.0.0
 */


if ( !defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

if( ! function_exists( 'ywrvp_campaign_build_link' ) ) {
	/**
	 * Build link of the product with Google Analytics options
	 *
	 * @since 1.0.4
	 * @param $link
	 * @return string
	 * @author Francesco Licandro
	 */
	function ywrvp_campaign_build_link( $link ) {

		if ( get_option( 'yith-wrvp-enable-analytics' ) == 'yes' ) {

			$campaign_source  = str_replace( ' ', '%20', get_option( 'yith-wrvp-campaign-source' ) );
			$campaign_medium  = str_replace( ' ', '%20', get_option( 'yith-wrvp-campaign-medium' ) );
			$campaign_term    = str_replace( ',', '+', get_option( 'yith-wrvp-campaign-term' ) );
			$campaign_content = str_replace( ' ', '%20', get_option( 'yith-wrvp-campaign-content' ) );
			$campaign_name    = str_replace( ' ', '%20', get_option( 'yith-wrvp-campaign-name' ) );

			$query_args = array(
					'utm_source' => $campaign_source,
					'utm_medium' => $campaign_medium,
			);

			if ( $campaign_term != '' ) {

				$query_args['utm_term'] = $campaign_term;

			}

			if ( $campaign_content != '' ) {

				$query_args['utm_content'] = $campaign_content;

			}

			$query_args['utm_name'] = $campaign_name;

			$link = add_query_arg( $query_args, $link );

		}

		return apply_filters( 'yith_wrvp_campaign_build_link', $link );
	}
}