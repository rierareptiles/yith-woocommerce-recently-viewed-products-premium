<?php
/**
 * Mail Handler class
 *
 * @author Yithemes
 * @package YITH WooCommerce Recently Viewed Products
 * @version 1.0.0
 */

if ( ! defined( 'YITH_WRVP' ) ) {
	exit;
} // Exit if accessed directly

if ( ! class_exists( 'YITH_WRVP_Mail_Handler' ) ) {
	/**
	 * Frontend class.
	 * The class manage all the frontend behaviors.
	 *
	 * @since 1.0.0
	 */
	class YITH_WRVP_Mail_Handler {

		/**
		 * Single instance of the class
		 *
		 * @var \YITH_WRVP_Mail_Handler
		 * @since 1.0.0
		 */
		protected static $instance;

		/**
		 * Plugin version
		 *
		 * @var string
		 * @since 1.0.0
		 */
		public $version = YITH_WRVP_VERSION;

		/**
		 * User meta last login
		 *
		 * @var string
		 * @since 1.0.0
		 */
		public $_user_meta_login = 'yith_wrvp_last_login';

		/**
		 * User meta exclude from mailing list
		 *
		 * @var string
		 * @since 1.0.0
		 */
		public $_user_meta_exclude = 'yith_wrvp_exclude_mail';

		/**
		 * User meta last login
		 *
		 * @var string
		 * @since 1.0.0
		 */
		public $_user_meta_ip_address = 'yith_wrvp_user_ip';

		/**
		 * User meta mail sent
		 *
		 * @var string
		 * @since 1.0.0
		 */
		public $_mail_sent = 'yith_wrvp_mail_sent';

		/**
		 * Returns single instance of the class
		 *
		 * @return \YITH_WRVP_Mail_Handler
		 * @since 1.0.0
		 */
		public static function get_instance(){
			if( is_null( self::$instance ) ){
				self::$instance = new self();
			}

			return self::$instance;
		}

		/**
		 * Constructor
		 *
		 * @access public
		 * @since 1.0.0
		 */
		public function __construct() {

			// unsubscribe user from mailing list
			add_action( 'init', array( $this, 'unsubscribe_from_mailing_list' ), 1 );

			// save user last visit time
			add_action( 'init', array( $this, 'save_user_last_visit' ), 10 );

			add_action( 'init', array( $this, 'mail_setup_schedule' ), 20 );
			add_action( 'mail_action_schedule', array( $this, 'mail_do_action_schedule' ) );

			// Email Templates custom Styles
			add_action( 'yith_wcet_after_email_styles', array( $this, 'email_templates_custom_css' ),10,3 );

			// add test mail
			add_action( 'woocommerce_email_settings_after', array( $this, 'add_test_mail' ), 10, 1 );
			add_action( 'yith_wrvp_mail_after_save_option', array( $this, 'send_test_mail' ), 10 );

			add_action( 'yith_wrvp_mail_sent_correctly', array( $this, 'set_meta_mail_sent' ), 10 );
		}

		/**
		 * Add custom styles for Email Templates
		 *
		 * @param int $premium_style
		 * @param array $meta
		 * @param WC_Email $current_email
		 */
		public function email_templates_custom_css( $premium_style, $meta, $current_email ) {
			if ( $current_email->id != 'yith_wrvp_mail' )
				return;

			$args = array( 'hide_style_for_email_templates' => true );
			wc_get_template( 'ywrvp-mail-style.php', $args, '', YITH_WRVP_TEMPLATE_PATH . '/email/' );
		}

		/**
		 * Save users last visit timestamp
		 *
		 * @access public
		 * @since 1.0.0
		 * @author Francesco Licandro
		 */
		public function save_user_last_visit() {

			$user_id = get_current_user_id();
			$date = time();

			if( ! $user_id ) {
				// find user from IP
				$user_id = $this->find_user_from_ip();
			}

			if( ! $user_id ) {
				return;
			}

			update_user_meta( $user_id, $this->_user_meta_login, $date );
			if( class_exists('WC_Geolocation') ) {
				update_user_meta($user_id, $this->_user_meta_ip_address, WC_Geolocation::get_ip_address() );
			}
			// reset mail sent
			delete_user_meta( $user_id, $this->_mail_sent );
		}

		/**
		 * Find an user from your IP
		 *
		 * @access public
		 * @since 1.0.1
		 * @return int
		 * @author Francesco Licandro
		 */
		public function find_user_from_ip() {
			global $wpdb;

			if( ! class_exists( 'WC_Geolocation' ) ) {
				return 0;
			}

			$ip = WC_Geolocation::get_ip_address();

			if( ! $ip ) {
				return 0;
			}

			$query           = array();
			$query['fields'] = "SELECT a.user_id FROM {$wpdb->usermeta} a";
			$query['where'] = " WHERE a.meta_key = '{$this->_user_meta_ip_address}'";
			$query['where'] .= " AND a.meta_value = '{$ip}'";

			$user = $wpdb->get_row( implode( ' ', $query ), ARRAY_A );

			if( is_null( $user ) || ! isset( $user[ 'user_id' ] ) ) {
				return 0;
			}

			return $user['user_id'];
		}

		/**
		 * Set user meta mail sent
		 *
		 * @access public
		 * @since 1.0.0
		 * @param string $customer
		 * @author Francesco Licandro
		 */
		public function set_meta_mail_sent( $customer ) {

			$customer = get_user_by( 'email', $customer );

			if( ! $customer ) {
				return;
			}

			update_user_meta( $customer->ID, $this->_mail_sent, true );
		}

		/**
		 * Schedule event to send mail to users
		 *
		 * @return void
		 * @author Francesco Licandro <francesco.licandro@yithemes.com>
		 */
		public function mail_setup_schedule() {
			if ( ! wp_next_scheduled( 'mail_action_schedule' ) ) {
				wp_schedule_event( time(), 'daily', 'mail_action_schedule' );
			}
		}

		/**
		 * Action send mail to users
		 *
		 * @access public
		 * @since 1.0.0
		 * @author Francesco Licandro
		 */
		public function mail_do_action_schedule() {

			global $wpdb;

			$data = array();

			// first get users and products list
			$query = $this->build_query();
			$results = $wpdb->get_results( implode( ' ', $query ) );

			if( empty( $results ) ) {
				return;
			}

			foreach( $results as $result ) {

				if( get_user_meta( $result->ID, $this->_user_meta_exclude, true ) || get_user_meta( $result->ID, $this->_mail_sent, true ) || user_can( $result->ID, 'administrator' ) || apply_filters( 'yith_wrvp_customer_skip_mail', false, $result->ID ) ){
					continue;
				}

				$data[ $result->user_email ] = maybe_unserialize( $result->meta_value );
			}

			do_action( 'send_yith_wrvp_mail', $data );
		}

		/**
		 * Get users mail and products list from DB
		 *
		 * @access protected
		 * @since 1.0.0
		 * @author Francesco Licandro
		 */
		protected function build_query(){

			global $wpdb;

			// period
			$period = absint( get_option( 'yith-wrvp-email-period', 7 ) );
			$time = time() - ( $period * DAY_IN_SECONDS );

			$query           = array();
			$query['fields'] = "SELECT a.ID, a.user_email, b.meta_value FROM {$wpdb->users} a, {$wpdb->usermeta} b";
			$query['join']   = " INNER JOIN {$wpdb->usermeta} c ON ( c.user_id = b.user_id AND c.meta_key='{$this->_user_meta_login}' )";
			$query['where'] = " WHERE b.meta_key = 'yith_wrvp_products_list'";
			$query['where'] .= " AND b.meta_value NOT LIKE 'a:0:{}'";
			$query['where'] .= " AND b.user_id = a.ID";
			$query['where'] .= " AND c.meta_value > 0 AND c.meta_value < {$time}";

			$query['group'] = "";

			return $query;
		}

		/**
		 * Add test mail box
		 *
		 * @access public
		 * @since 1.0.0
		 * @param $mail
		 * @author Francesco Licandro
		 */
		public function add_test_mail( $mail ) {

			if( ! $mail || $mail->id != 'yith_wrvp_mail' ){
				return;
			}

			ob_start();
			?>

			<table class="form-table">
				<tbody>
					<tr valign="top">
						<th scope="row" class="titledesc">
							<label for="yith-wrvp-test-mail"><?php _e( 'Test Email', 'yith-woocommerce-recently-viewed-products' ) ?></label>
						</th>
						<td>
							<input type="email" id="yith-wrvp-test-mail" name="yith-wrvp-test-mail"
								   placeholder="<?php _e( 'Type an email address to send a test email', 'yith-woocommerce-recently-viewed-products' ) ?>" />
							<input type="hidden" name="is-yith-wrvp-test-mail" value="">
							<button type="submit" class="button-secondary ywrvp-send-test-email"><?php _e( 'Send email', 'yith-woocommerce-recently-viewed-products' ) ?></button>
						</td>
					</tr>
				</tbody>
			</table>

			<?php

			echo ob_get_clean();
		}

		/**
		 * Send test mail action
		 *
		 * @access public
		 * @since 1.0.0
		 * @author Francesco Licandro
		 */
		public function send_test_mail(){

			if( ! ( isset( $_POST['yith-wrvp-test-mail'] ) && $_POST['yith-wrvp-test-mail'] != '' )
				|| ! ( isset( $_POST['is-yith-wrvp-test-mail' ] ) && $_POST['is-yith-wrvp-test-mail'] == 'true' ) ) {
				return;
			}

			// check also if is mail correct
			if( ! is_email( $_POST['yith-wrvp-test-mail'] ) ) {
				return;
			}

			$data[ $_POST['yith-wrvp-test-mail'] ] = array();

			do_action( 'send_yith_wrvp_mail', $data );

		}

		/**
		 * Create coupon for the email
		 *
		 * @access public
		 * @since 1.0.0
		 * @param string $user
		 * @param array $products
		 * @param string $expire
		 * @param string $value
		 * @return string
		 * @author Francesco Licandro
		 */
		public function add_coupon_to_mail( $user, $products, $expire, $value ) {

			if( ! $value || ! $expire ) {
				return '';
			}

			// make sure expire and value is number positive
			$value 	= abs( $value );
			$expire = abs( $expire );

			$prefix = apply_filters( 'yith_wrvp_prefix_coupon', 'ywrvp_' );
			$coupon_code   = uniqid( $prefix ); // Code

			$coupon = array(
				'post_title' => $coupon_code,
				'post_content' => '',
				'post_status' => 'publish',
				'post_author' => 1,
				'post_type'		=> 'shop_coupon'
			);

			$new_coupon_id = wp_insert_post( $coupon );

			update_post_meta( $new_coupon_id, 'discount_type', 'percent_product' );
			update_post_meta( $new_coupon_id, 'coupon_amount', $value );
			update_post_meta( $new_coupon_id, 'individual_use', 'yes' );
			update_post_meta( $new_coupon_id, 'product_ids', implode( ',', $products ) );
			update_post_meta( $new_coupon_id, 'exclude_product_ids', '' );
			update_post_meta( $new_coupon_id, 'usage_limit', '1' );
			update_post_meta( $new_coupon_id, 'usage_limit_per_user', '0' );
			update_post_meta( $new_coupon_id, 'limit_usage_to_x_items', '1' );
			update_post_meta( $new_coupon_id, 'expiry_date', date( 'Y-m-d', $expire ) );
			update_post_meta( $new_coupon_id, 'free_shipping', 'no' );

			return $coupon_code;

		}

		/**
		 * Add success/fail message after send test mail
		 *
		 * @access public
		 * @since 1.0.0
		 * @param object $email
		 * @author Francesco Licandro
		 */
		public function add_test_mail_message( $email ){

			if( ! isset( $_GET['page'] ) || $_GET['page'] != 'yith_wrvp_panel' ) {
				return;
			}

			$type = $email->_test_msg_type;

			$notice = $type == 'success' ? __( 'Test email sent correctly!', 'yith-woocommerce-recently-viewed-products' ) : __( 'An error has occurred. Please try again.', 'yith-woocommerce-recently-viewed-products' );

			$notice = apply_filters( 'yith_wrvp_test_mail_' . $type . '_notice', $notice );

			echo '<div class="notice notice-' . $type . '"><p>' . $notice . '</p></div>';
		}

		/**
		 * Parse args with default options for options type
		 *
		 * @access public
		 * @since 1.0.0
		 * @param array $data
		 * @return array
		 * @author Francesco Licandro
		 */
		public function parse_with_default( $data ) {

			$defaults = array(
				'title'             => '',
				'disabled'          => false,
				'class'             => '',
				'css'               => '',
				'placeholder'       => '',
				'type'              => 'text',
				'desc_tip'          => false,
				'description'       => '',
				'custom_attributes' => array()
			);

			return wp_parse_args( $data, $defaults );
		}

		/**
		 * Print select products html for email options
		 *
		 * @access public
		 * @since 1.0.0
		 * @param string $key
		 * @param array $data
		 * @param object $email
		 * @return string
		 * @author Francesco Licandro
		 */
		public function select_products_html( $key, $data, $email ) {

			$field  = $email->get_field_key( $key );
			$data 	= $this->parse_with_default( $data );

            $products = $email->get_option( $key );
            ! is_array( $products ) && $products = explode( ',', $products );
            // remove empty
            $products = array_filter( $products );
            $json_ids    = array();

            foreach ( $products as $product_id ) {
                $product = wc_get_product( $product_id );
                if ( is_object( $product ) ) {
                    $json_ids[ $product_id ] = wp_kses_post( html_entity_decode( $product->get_formatted_name() ) );
                }
            }

			ob_start();
			?>

			<tr valign="top">
				<th scope="row" class="select_products">
					<label for="<?php echo esc_attr( $field ); ?>"><?php echo wp_kses_post( $data['title'] );  ?></label>
					<?php echo $email->get_tooltip_html( $data ); ?>
				</th>
				<td class="forminp">
					<fieldset>
						<legend class="screen-reader-text"><span><?php echo wp_kses_post( $data['title'] ); ?></span></legend>
						<div id="<?php echo esc_attr( $field ); ?>-container">
							<div class="option">
                                <?php
                                yit_add_select2_fields( array(
                                    'class'         => 'wc-product-search',
                                    'id'            => esc_attr( $field ),
                                    'name'          => esc_attr( $field ),
                                    'style'         => 'width:50%;',
                                    'data-multiple' => true,
                                    'data-selected' => $json_ids,
                                    'value'         => $products

                                ) );
                                ?>
								<?php echo $email->get_description_html( $data ); ?>
							</div>
						</div>
					</fieldset>
				</td>
			</tr>

			<?php

			return ob_get_clean();
		}

		/**
		 * Print textarea editor html for email options
		 *
		 * @access public
		 * @since 1.0.0
		 * @param string $key
		 * @param array $data
		 * @param object $email
		 * @return string
		 * @author Francesco Licandro
		 */
		public function textarea_editor_html( $key, $data, $email ) {

			$field  = $email->get_field_key( $key );
			$data 	= $this->parse_with_default( $data );

			$editor_args = array(
				'wpautop'       => true, // use wpautop?
				'media_buttons' => true, // show insert/upload button(s)
				'textarea_name' => esc_attr( $field ), // set the textarea name to something different, square brackets [] can be used here
				'textarea_rows' => 20, // rows="..."
				'tabindex'      => '',
				'editor_css'    => '', // intended for extra styles for both visual and HTML editors buttons, needs to include the <style> tags, can use "scoped".
				'editor_class'  => '', // add extra class(es) to the editor textarea
				'teeny'         => false, // output the minimal editor config used in Press This
				'dfw'           => false, // replace the default fullscreen with DFW (needs specific DOM elements and css)
				'tinymce'       => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
				'quicktags'     => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
			);

			ob_start();
			?>

			<tr valign="top">
				<th scope="row" class="select_categories">
					<label for="<?php echo esc_attr( $field ); ?>"><?php echo wp_kses_post( $data['title'] );  ?></label>
					<?php echo $email->get_tooltip_html( $data ); ?>
				</th>
				<td class="forminp">
					<fieldset>
						<div id="<?php echo esc_attr( $field ); ?>-container">
							<div class="editor"><?php wp_editor( $email->get_option( $key ), esc_attr( $field ), $editor_args ); ?></div>
							<?php echo $email->get_description_html( $data ); ?>
						</div>
					</fieldset>
				</td>
			</tr>

			<?php

			return ob_get_clean();
		}

		/**
		 * Print upload type html for email options
		 *
		 * @access public
		 * @since 1.0.0
		 * @param string $key
		 * @param array $data
		 * @param object $email
		 * @return string
		 * @author Francesco Licandro
		 */
		public function upload_html( $key, $data, $email ) {

			$field  = $email->get_field_key( $key );
			$data 	= $this->parse_with_default( $data );

			ob_start();
			?>

			<tr valign="top">
				<th scope="row">
					<label for="<?php echo esc_attr( $field ); ?>"><?php echo wp_kses_post( $data['title'] );  ?></label>
					<?php echo $email->get_tooltip_html( $data ); ?>
				</th>
				<td class="forminp">
					<fieldset>
						<div id="<?php echo esc_attr( $field ); ?>-container" class="plugin-option">
							<input type="text" name="<?php echo esc_attr( $field ); ?>" id="<?php echo esc_attr( $field ); ?>" value="<?php echo $email->get_option( $key ); ?>" class="upload_img_url" style="width: 25em;" />
							<input type="button" value="<?php _e( 'Upload', 'yith-plugin-fw' ) ?>" id="<?php echo esc_attr( $field ); ?>-button" class="upload_button button" />
							<?php echo $email->get_description_html( $data ); ?>
						</div>
						<div class="upload_img_preview yith-wrvp" style="margin-top:10px;">
							<?php
							$file = $email->get_option( $key );
							if ( preg_match( '/(jpg|jpeg|png|gif|ico)$/', $file ) ) {
								echo '<img src="' . $file .'" />';
							}
							?>
						</div>
					</fieldset>
				</td>
			</tr>

			<?php

			return ob_get_clean();
		}

		/**
		 * Unsubscribe user from mailing list
		 *
		 * @access public
		 * @since 1.0.0
		 * @author Francesco Licandro
		 */
		public function unsubscribe_from_mailing_list(){
			if( ! isset( $_GET['action'] ) || $_GET['action'] !== 'yith_wrvp_unsubscribe_from_list' || ! isset( $_GET['customer'] ) ){
				return;
			}

			$user_id = $this->find_user_md5( $_GET['customer'] );

			if( is_null( $user_id ) ){
				return;
			}

			update_user_meta( $user_id, $this->_user_meta_exclude, true );

			if( is_user_logged_in() ) {
				wc_add_notice( __( 'You have been unsubscribed from our mailing list successfully.', 'yith-woocommerce-recently-viewed-products' ), 'success' );
				$url = wc_get_page_permalink( 'myaccount' );
			}
			else {
				$url = home_url();
			}

			wp_safe_redirect( $url );
			exit();
		}

		/**
		 * Find user by md5 id
		 *
		 * @since 1.0.0
		 * @author Francesco Licandro
		 */
		public function find_user_md5( $md5_id ) {

			global $wpdb;

			$query           = array();
			$query['fields'] = "SELECT a.ID FROM {$wpdb->users} a";
			$query['where'] = " WHERE MD5(a.ID) = '$md5_id'";

			$query = apply_filters( 'yith_wrvp_query_md5user_param', $query );

			$results = $wpdb->get_var( implode( ' ', $query ) );

			return $results;
		}
	}
}
/**
 * Unique access to instance of YITH_WRVP_Mail_Handler class
 *
 * @return \YITH_WRVP_Mail_Handler
 * @since 1.0.0
 */
function YITH_WRVP_Mail_Handler(){
	return YITH_WRVP_Mail_Handler::get_instance();
}