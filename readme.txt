= 1.1.0 = Released on Apr 04, 2017

* New: Support to WooCommerce 3.0.0.
* New: Support to WordPress 4.7.3.
* Update: Plugin Core.
* Update: Language file.
* Fix: Missing Autoplay Speed param on products slider.
* Dev: Add filter "yith_wrvp_coupon_code_image_email" for filter coupon image on email.
* Dev: Add filter "yith_wrvp_coupon_code_html_email" for filter coupon code html on email.
* Dev: Add filter "yith_wrvp_unsubscribe_link_url" for filter unsubscribe link return url.

= 1.0.4 = Released on Feb 16, 2016

* New: Integration of Google Analytics campaign for email link.
* New: Option for choose to show the shortcode on single product page
* Update: Plugin Core
* Update: Language file

= 1.0.3 = Released on Feb 05, 2016

* New: Compatibility with YITH WooCommerce Email Template Premium.
* Update: Plugin Core

= 1.0.2 = Released on Jan 14, 2016

* New: Compatibility with WooCommerce 2.5 RC.
* Update: Plugin Core
* Update: Language file
* Fix: Responsive for products slider

= 1.0.1 = Released on Nov 30, 2015

* New: Link in email for unsubscribe to mailing list
* New: Track visit with IP for no logged in customer
* New: Option for get similar products by tags, categories ot both
* New: Specify View All link url instead of standard page link
* Update: Plugin Core
* Update: Language file
* Fix: Scheduled emails are sent multiple times to customer

= 1.0.0 = Released on Oct 13, 2015

* Initial release