<?php
/**
 * YITH WooCommerce Recently Viewed Products
 */

if (!defined('YITH_WRVP')) {
    exit; // Exit if accessed directly
}

$loop = 0;

$size   = get_option( 'yith-wrvp-image-size', '' );
$width  = isset( $size['width'] ) ? $size['width'] : 80;
$height = isset( $size['height'] ) ? $size['height'] : 80;

?>

<table id="ywrvp-products-list">
    <tbody>

    <?php while ( $products->have_posts() ) : $products->the_post();

        $product_id = get_the_ID();
        $product_link = get_permalink( $product_id );
        $product_link = ywrvp_campaign_build_link( $product_link );
        
        $product = wc_get_product( $product_id );
        
        if( defined('YITH_WCWL') && YITH_WCWL ) {
            $wishlist_link = ywrvp_campaign_build_link( YITH_WCWL()->get_wishlist_url() );
        }


        $loop++;        
        $class = ( $loop == $products->post_count ) ? 'last' : '';
        ?>

        <tr>
            <td class="ywrvp-product <?php echo $class ?>">
                <table id="ywrvp-product-info">
                    <tbody>
                    <tr>
                        <td class="product-image">
                            <?php
                                $src = ( $product->get_image_id() ) ? current( wp_get_attachment_image_src( $product->get_image_id(), 'ywrvp_image_size' ) ) : wc_placeholder_img_src();

                                echo '<a href="' . $product_link .'"><img src="'. $src . '" height="' . $height . '" width="' . $width . '" /></a>';
                           ?>
                        </td>

                        <td class="product-info">

                            <h3>
                                <a href="<?php echo $product_link ?>">
                                    <?php the_title() ?>
                                </a>
                            </h3>

                            <div>
                                <?php wc_get_template('loop/price.php'); ?>
                            </div>

                        </td>

                        <td class="product-action">
                            <div><a href="<?php echo $product_link ?>" class="mail-button"><?php _e('View Details', 'yith-woocommerce-recently-viewed-products' ) ?></a></div>
                            <?php if( defined('YITH_WCWL') && YITH_WCWL ) : ?>
                                <div><a href="<?php echo esc_url( add_query_arg( 'add_to_wishlist', $product_id, $wishlist_link ) )?>" class="mail-button"><?php _e( 'Add to wishlist', 'yith-woocommerce-recently-viewed-products' ) ?></a></div>
                            <?php endif; ?>
                        </td>

                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>

    <?php endwhile; // end of the loop. ?>

    </tbody>
</table>