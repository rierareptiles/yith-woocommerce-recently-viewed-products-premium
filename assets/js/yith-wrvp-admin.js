/**
 * admin.js
 *
 * @author Your Inspiration Themes
 * @package YITH WooCommerce Recently Viewed Products
 * @version 1.0.0
 */

jQuery(document).ready(function ($) {
    "use strict";

    var select = $(document).find('.yith-wrvp-shortcode-tab select'),
        shortcode_option = $(document).find( 'input.shortcode-option, select.shortcode-option' ),
        preview = $( '.shortcode-preview' );

    shortcode_option.each( function(){

        $(this).on( 'change', function(){

            var value = ( this.type == 'checkbox' && ! $(this).is( ':checked' ) ) ? $(this).data('novalue') : $(this).val(),
                name  = this.name.replace( 'yith_', '').replace( '[]', '' ),
                shortcode = preview.html(),
                attr;

            // remove old
            var reg = new RegExp( name + '="([^"]*)"', 'g' );
            shortcode = shortcode.replace( reg, '' );

            if( ! value || ! $(this).closest('tr').is(':visible') ) {
                preview.html( shortcode );
                return;
            }

            // else add attr
            shortcode = shortcode.replace(']', '');

            attr = name + '="' + value + '"';
            preview.html( shortcode + ' ' + attr + ']' )

        });
    });


    $(document).on( 'click', 'button.ywrvp-send-test-email', function(){
        $(this).prev().val(true);
    });



    /*############################
        DEPS ADMIN PANEL
    ##############################*/

    var input = $(document).find('select, input');

    input.each( function(){
        var i      = $(this),
            option = i.closest('tr'),
            parent,
            deps = i.data('deps'),
            deps_value = i.data('deps_value');

        if( typeof deps_value == 'undefined' || typeof deps == 'undefined' ){
            return;
        }

        parent = $(document).find( '#' + deps );

        parent.on('change', function(){

            if( ( i.attr('type') != 'checkbox' && deps_value == $(this).val() ) || ( deps_value == 'yes' && $(this).is(':checked') ) || ( deps_value == 'no' && ! $(this).is(':checked') ) ) {
                option.show();
            }
            else{
                option.hide();
            }

            i.trigger('change');
        });

    });

    input.trigger('change');


    /*##########################
      CUSTOM CHECKLIST
    ###########################*/

    var array_unique_noempty, element_box;

    array_unique_noempty = function (array) {
        var out = [];

        $.each(array, function (key, val) {
            val = $.trim(val);

            if (val && $.inArray(val, out) === -1) {
                out.push(val);
            }
        });

        return out;
    };

    element_box = {
        clean: function (tags) {

            tags = tags.replace(/\s*,\s*/g, ',').replace(/,+/g, ',').replace(/[,\s]+$/, '').replace(/^[,\s]+/, '');

            return tags;
        },

        parseTags: function (el) {
            var id = el.id,
                num = id.split('-check-num-')[1],
                element_box = $(el).closest('.ywrvp-checklist-div'),
                values = element_box.find('.ywrvp-values'),
                current_values = values.val().split(','),
                new_elements = [];

            delete current_values[num];

            $.each(current_values, function (key, val) {
                val = $.trim(val);
                if (val) {
                    new_elements.push(val);
                }
            });

            values.val(this.clean(new_elements.join(',')));

            this.quickClicks(element_box);
            return false;
        },

        quickClicks: function (el) {

            var values = $('.ywrvp-values', el),
                values_list = $('.ywrvp-value-list ul', el),

                id = $(el).attr('id'),
                current_values;

            if (!values.length)
                return;

            current_values = values.val().split(',');
            values_list.empty();

            $.each(current_values, function (key, val) {

                var item, xbutton;

                val = $.trim(val);

                if (!val)
                    return;

                item = $('<li class="select2-search-choice" />');
                xbutton = $('<a id="' + id + '-check-num-' + key + '" class="select2-search-choice-close" tabindex="0"></a>');

                xbutton.on('click keypress', function (e) {

                    if (e.type === 'click' || e.keyCode === 13) {

                        if (e.keyCode === 13) {
                            $(this).closest('.ywrvp-checklist-div').find('input.ywrvp-insert').focus();
                        }

                        element_box.parseTags(this);
                    }

                });

                item.prepend('<div><div class="selected-option" data-id="' + val + '">' + val + '</div></div>').prepend(xbutton);

                values_list.append(item);

            });
        },

        flushTags: function (el, a, f) {

            var current_values,
                new_values,
                text,
                values = $('.ywrvp-values', el),
                add_new = $('input.ywrvp-insert', el);

            a = a || false;

            text = a ? $(a).text() : add_new.val();

            if ('undefined' == typeof( text )) {
                return false;
            }

            current_values = values.val();
            new_values = current_values ? current_values + ',' + text : text;
            new_values = this.clean(new_values);
            new_values = array_unique_noempty(new_values.split(',')).join(',');
            values.val(new_values);

            this.quickClicks(el);

            if (!a)
                add_new.val('');
            if ('undefined' == typeof( f ))
                add_new.focus();

            return false;

        },

        init: function () {
            var ajax_div = $('.ywrvp-checklist-ajax');

            $('.ywrvp-checklist-div').each(function () {
                element_box.quickClicks(this);
            });

            $('input.ywrvp-insert', ajax_div).keyup(function (e) {
                if (13 == e.which) {
                    element_box.flushTags( $(this).closest('.ywrvp-checklist-div') );
                    return false;
                }
            }).keypress(function (e) {
                if (13 == e.which) {
                    e.preventDefault();
                    return false;
                }
            });
        }
    };

    element_box.init();

});